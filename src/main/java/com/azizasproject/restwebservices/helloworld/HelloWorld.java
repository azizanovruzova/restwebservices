package com.azizasproject.restwebservices.helloworld;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import java.util.Locale;

@RestController
public class HelloWorld {

    @Autowired
    private MessageSource messageSource;

    @GetMapping(path = "/helloworld")
    public String showHelloWorld() {
        return "Hello, world!";
    }

    @GetMapping(path="/hello-world-bean")
    public HelloWorldBean showHelloWorldBean() {
        return new HelloWorldBean("Hello, World!");
    }

    @GetMapping(path="/hello-world-internationalized")
    public String showHelloWorldInternationalized(@RequestHeader() Locale locale) {
        return messageSource.getMessage("hello.world.message", null, LocaleContextHolder.getLocale());
    }
}
