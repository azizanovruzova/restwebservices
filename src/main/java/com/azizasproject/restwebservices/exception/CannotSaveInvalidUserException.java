package com.azizasproject.restwebservices.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class CannotSaveInvalidUserException extends RuntimeException {
    public CannotSaveInvalidUserException(String message) {
        super(message);
    }
}
