package com.azizasproject.restwebservices.user;

import com.azizasproject.restwebservices.post.Post;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@ApiModel(description = "All details about the user.")
@Entity
public class User {

    @Id
    @GeneratedValue
    private int id;

    @Size(min=3, max = 15, message = "Name should be at least 3 characters long and 15 at max.")
    @ApiModelProperty(notes = "Name should be at least 3 characters.")
    private String name;

    @ApiModelProperty(notes = "Birth Date should be in the past")
    private Date birthDay;



    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<Post> posts;

    protected User(){

    }

    public User(int id, String name, Date birthDay) {
        this.id = id;
        this.name = name;
        this.birthDay = birthDay;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(Date birthDay) {
        this.birthDay = birthDay;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", birthDay=" + birthDay +
                '}';
    }
}
