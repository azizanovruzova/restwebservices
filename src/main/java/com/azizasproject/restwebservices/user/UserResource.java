package com.azizasproject.restwebservices.user;

import com.azizasproject.restwebservices.exception.CannotSaveInvalidUserException;
import com.azizasproject.restwebservices.exception.UserNotFoundException;
import com.azizasproject.restwebservices.exception.UsersListEmptyException;
import com.azizasproject.restwebservices.post.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
public class UserResource {

     @Autowired
     private UserService service;

    @GetMapping("/users")
    public List<User> getAllUsers() {
        if (service.findAll().isEmpty()) {
            throw new UsersListEmptyException("Users not found in the list");
        }
        return service.findAll();
    }

    @GetMapping("/users/{id}")
    public Resource<User> getUser(@PathVariable int id) {
        User user = service.findUserById(id);
        if (user == null) {
            throw new UserNotFoundException("id=" + id);
        }
        Resource<User> userResource = new Resource<User>(user);

        ControllerLinkBuilder linkTo = ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(this.getClass()).getAllUsers());

        userResource.add(linkTo.withRel("all-users"));

        return userResource;
    }

    @PostMapping("/users")
    public ResponseEntity<Object> createUser(@Valid @RequestBody User user) {
        User savedUser = service.save(user);

        if (savedUser == null) {
            throw new CannotSaveInvalidUserException("Can not accept null as a user");
        }

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(savedUser.getId())
                .toUri();

        return ResponseEntity.created(location).build();
        //ResponseEntity.created()
    }

    @DeleteMapping("users/{id}")
    public void deleteUser(@PathVariable int id) {
        if (service.findUserById(id) == null) {
            throw new UserNotFoundException("id=" + id);
        }
        service.deleteUserById(id);

    }

}
